"use strict";

const fs = require("fs");
let execs = 0;
process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = [];
let currentLine = 0;

// process.stdin.on("data", function (inputStdin) {
//   inputString += inputStdin;
// });

// process.stdin.on("end", function () {
//   inputString = inputString.split("\n");

//   main();
// });
let results = [],
  r_index = -1;

(() => {
  fs.readFileSync("./200000-2.txt")
    .toString()
    .split("\n")
    .forEach((line, i) => {
      if (line.includes("Expected")) {
        r_index = i;
      }
      if (i > r_index && r_index > -1) {
        results.push(line);
      } else {
        inputString.push(line);
      }
    });
  main();
})();

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'climbingLeaderboard' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts following parameters:
 *  1. INTEGER_ARRAY ranked
 *  2. INTEGER_ARRAY player
 */


function check_position(player, last_score, last_pos) {
  if (player.length === 2) {
    if (player[1] < last_score) {
      return last_pos + 1;
    } else {
      return last_pos;
    }
  } else {
    let middle = Math.round(player.length / 2) - 1;
    if (last_score >= player[middle]) {
      return check_position(player.slice(middle, player.length), last_score, last_pos + middle + 1);
    } else {
      return check_position(player.slice(0, middle + 1), last_score, last_pos);
    }
  }
}

function check_position_2(player, first_score, first_pos) {
  if (player.length === 2) {
    if (first_score > player[0]) {
      return first_pos + 1;
    } else {
      return first_pos;
    }
  } else {
    let middle = Math.round(player.length / 2) - 1;
    if (first_score >= player[middle]) {
      return check_position_2(player.slice(middle, player.length), first_score, first_pos + middle);
    } else {
      return check_position_2(player.slice(0, middle + 1), first_score, first_pos);
    }
  }
}

function recursive(ranked, score, last_pos) {
  if (score >= ranked[last_pos] && score < ranked[last_pos - 1]) {
    ranked[last_pos] = score;
    return { last_pos, ranked };
  } else {
    last_pos = last_pos - 1;
    return recursive(ranked, score, last_pos);
  }
}

function climbingLeaderboard(ranked, player) {
  // Write your code here
  ranked = [...new Set(ranked)];

  let tmp_ranked_player_scores = [];
  let positions = [];
  let first_positions = [];
  let last_positions = [];
  let index_of_smallest_ranked_score_in_player = 0;

  if (player[player.length - 1] >= ranked[0]) {
    let index = check_position_2(player, ranked[0], 0);
    tmp_ranked_player_scores = player.slice(index, player.length);
    first_positions = tmp_ranked_player_scores.fill(1);
    player = player.slice(0, index);
  }

  if (player[0] < ranked[ranked.length - 1]) {
    index_of_smallest_ranked_score_in_player = check_position(player, ranked[ranked.length - 1], 0);
    tmp_ranked_player_scores = player.slice(0, index_of_smallest_ranked_score_in_player + 1);
    last_positions = tmp_ranked_player_scores.fill(ranked.length+1)
    player = player.slice(index_of_smallest_ranked_score_in_player + 1, player.length);
  }
  let last_pos = ranked.length - 1;
  positions = [
    ...last_positions,
    ...player.map((score, i) => {
      let result = recursive(ranked, score, last_pos);
      ranked = result.ranked;
      last_pos = result.last_pos;
      let pos = last_pos + 1;
      return pos;
    }),
    ...first_positions,
  ];

  return positions;
}

function main() {
  // const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const rankedCount = parseInt(readLine().trim(), 10);

  const ranked = readLine()
    .replace(/\s+$/g, "")
    .split(" ")
    .map((rankedTemp) => parseInt(rankedTemp, 10));

  const playerCount = parseInt(readLine().trim(), 10);

  const player = readLine()
    .replace(/\s+$/g, "")
    .split(" ")
    .map((playerTemp) => parseInt(playerTemp, 10));

  console.time("climbingLeaderboard");
  const result = climbingLeaderboard(ranked, player);
  console.timeEnd("climbingLeaderboard");

  // console.log(result.join("\n") + "\n");
  // console.log(results.join("\n") + "\n");
  results.forEach((r, i) => {
    if (parseInt(result[i]) !== parseInt(r)) {
      console.log("Divergent line : " + (i + 1), r, result[i]);
    }
  });
  console.log(results.join("\n") === result.join("\n"));
  // ws.write(result.join("\n") + "\n");

  // ws.end();
}
